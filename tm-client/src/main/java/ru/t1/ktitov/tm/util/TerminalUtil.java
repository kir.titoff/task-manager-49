package ru.t1.ktitov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.exception.AbstractException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        try {
            @NotNull final String value = nextLine();
            return Integer.parseInt(value);
        } catch (@NotNull final Exception e) {
            throw new InvalidNumberException();
        }
    }

    final class InvalidNumberException extends AbstractException {
        public InvalidNumberException() {
            super("Error! Value is not a number.");
        }
    }

}
