package ru.t1.ktitov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    private final ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(@NotNull final String json) {
        final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        final String table = event.get("table").toString();
        System.out.println(json);
        final byte[] bytes = json.getBytes();
        final String pathname = "./log-data/" + table;
        @NotNull final File file = new File(pathname);
        @NotNull final Path path = file.toPath();
        if (!Files.exists(path.getParent())) {
            Files.createDirectories(path.getParent());
            Files.createFile(path);
        }
        Files.write(Paths.get(pathname), bytes, StandardOpenOption.APPEND);
    }

}
